# Welltory_test

Ответы на вопросы:

* [Доступы к s3](./access/README.md)
* [Анонимизация базы](./database/README.md)
* [Отслеживание утечек данных](./datalost/README.md)
* [Математические модели](./math/README.md)
* [Хранение промежуточных и финальных результатов работы](./storage/README.md)
* [Версионирование кода и моделей](./model_vcs/README.md)