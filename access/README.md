# Разграничение доступов

Разделение доступов к Rds/S3 происходит засчет политик:
1. Создается пользователь для разработчика
2. Создается ключ для доступа
3. Пользователь прикрепляется к роли 
4. Теперь только этот пользователь ( ну и те у кого есть полный доступ к S3) имеет возможность работать с этой папкой


```json
resource "aws_iam_user" "developer" {
  name = "developer"
  path = "/system/"

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_access_key" "developer" {
  user = aws_iam_user.developer.name
}

resource "aws_iam_user_policy" "developer_S3" {
  name = "developer role"
  user = aws_iam_user.developer.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": "arn:aws:s3:::developer-buckett"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": "arn:aws:s3:::developer-buckett/*"
    }
  ]
}
EOF
}

```
