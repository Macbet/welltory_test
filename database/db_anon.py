from diagrams import Cluster, Diagram
from diagrams.aws.storage import S3
from diagrams.aws.database import RDS
from diagrams.aws.compute import LambdaFunction


with Diagram("RDS anon", show=False):
    rds_prod = RDS("production_db")
    prod_backup = S3("prod_backup_storage")
    prod_anon = S3("anon_storage")
    

    with Cluster("Lambdas"):
        lambda_anon = LambdaFunction("anon_function")
        lambda_check = LambdaFunction("check_backup")

    with Cluster("Staging Rds"):
        rds_anon = [RDS("anonymized_db_1"),
                    RDS("anonymized_db_2"),
                    RDS("anonymized_db_3")]

    rds_prod >> prod_backup  
    prod_backup >> lambda_anon 
    lambda_anon >> prod_anon 
    prod_anon >> rds_anon
    lambda_check >> rds_anon