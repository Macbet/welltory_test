# Вычисления на CPU/GPU
Для оптимальных вычислений с помощью cpu/gpu приложения необходимо распределять по выделенным нодам.
В примере Terraform кода создается кластер EKS с нодами имеющими GPU и пролейблированными лейблом gpu=ready этот лейб мы можем использовать в манифесте K8s для распределения подов по нужным нодам.
Ниже пример манифеста которым мы задеплоим поды на определенные ноды.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-gpu2
  labels:
    node: gpu
spec:
  affinity:
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
          matchExpressions:
          - key: node
            operator: In
            values:
            - gpu
        topologyKey: kubernetes.io/hostname
  nodeSelector:
    gpu: ready
  containers:
  - name: pod-antiaffinity
    image: docker.io/example/hello-gpu
```    
^^^ вариант с nodeselector

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-gpu2
  labels:
    node: gpu
spec:
  affinity:
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
          matchExpressions:
          - key: node
            operator: In
            values:
            - gpu
        topologyKey: kubernetes.io/hostname
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: gpu
            operator: In
            values:
            - ready
  containers:
  - name: pod-antiaffinity
    image: docker.io/example/hello-gpu
``` 
^^^ вариант с nodeaffinity