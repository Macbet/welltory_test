variable "region" {
  default = "us-east-1"
}

variable "map_accounts" {
  type        = list(string)

  default = [
    "555555555555",
    "999999999999",
  ]
}

variable "map_roles" {
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::66666666666:role/devops"
      username = "devops"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::66666666666:user/qa"
      username = "qa"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::66666666666:user/devs"
      username = "devs"
      groups   = ["system:masters"]
    },
  ]
}